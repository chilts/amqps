# amqps #

An SQS-like queue using [amqplib](https://www.npmjs.com/package/amqplib) for RabbitMQ.

## Synopsis ##

```javascript
const amqplib = require('amqpslib')
const Queue = require('amqps')

// Params:
// 1. `amqplib`
// 2. Queue Name
// 3. RabbitMQ URL
// 4. Job timeout (in ms)
const queue = new Queue(amqplib, 'image-thumbnail', 'amqp://localhost', 30 * 1000)

// For producers AND consumers.
await queue.setup()
```

Producers:

```
const ok = await queue.sendText('filename.jpg')
if (!ok) {
  // ...
}
```

Consumers:

```
// listen for 'msg' events
queue.on('msg', (text, done) => {
  // text is 'filename.jpg'

  // eventually, call `done()` or `done(err)`
  done()
})

// tell RabbitMQ we want to be a consumer
queue.consume()
```

## Full Example ##

```
const amqplib = require('amqpslib')
const Queue = require('amqps')

// create a queue
const queue = new Queue(amqplib, 'image-thumbnail', 'amqp://localhost', 30 * 1000)

// Since we're using Async/Await, we need to wrap the top-level in an async function
// but you'll probably already be inside one anyway during program setup.
;(async () => {

  // set the queue up so we connect to RabbitMQ and create the queue
  await queue.setup()

  // listen for 'msg' events
  queue.on('msg', (text, done) => {
    try {
      // text is 'filename.jpg'
      await image.resize(filename.jpg)

      // eventually, call `done()` or `done(err)`
      done()
    }
    catch (err) {
      // ToDo: log the error
      // job will be sent to another consumer
      done(err)
    }
  })

  // tell RabbitMQ we want to be a consumer
  queue.consume()

  // and send the queue a message
  const ok = await queue.sendText('filename.jpg')
  if (!ok) {
    // ...
  }

})()
```

## RoadMap ##

To do:

* max number of retries
* dead-letter queues

## Author ##

```
$ npx chilts
```

```
   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛
```

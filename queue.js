// ----------------------------------------------------------------------------

// core
const EventEmitter = require('events')

// ----------------------------------------------------------------------------

class Queue extends EventEmitter {
  constructor(amqplib, name, url, timeout) {
    super()
    // console.log('Insite Queue.constructor()')
    this.amqplib = amqplib
    this.name = name
    this.url = url
    this.timeout = timeout
    this.conn = null
    this.channel = null
    this.isSetup = false
    this.consumer = null // looks like `{ consumerTag: 'amq.ctag-pZeo3K_LP6GJzKeaBxdq1Q' }`
    // this.err = null
  }

  async setup() {
    // connect
    this.conn = await this.amqplib.connect(this.url)
    // console.log('conn:', this.conn)
    this.emit('connected')

    // create the channel
    this.channel = await this.conn.createConfirmChannel()
    // console.log('channel:', this.channel)
    this.emit('channel-created')

    // create this queue
    const queue = await this.channel.assertQueue(this.name, { durable: true })
    // console.log('queue:', queue)

    this.isSetup = true
  }

  async sendText(text) {
    if ( !this.isSetup ) {
      throw new Error('sendText() : Not set up correctly')
    }
    return await this.channel.sendToQueue(this.name, new Buffer(text), { persistent: true })
  }

  async sendData(data) {
    if ( !this.isSetup ) {
      throw new Error('sendData() : Not set up correctly')
    }
    return await this.channel.sendToQueue(this.name, new Buffer(JSON.stringify(data)), { persistent: true })
  }

  // turn this instance into a consumer
  async consume() {
    // console.log('Consuming messages')
    if ( !this.isSetup ) {
      throw new Error('consume() : Not set up correctly')
    }

    // consume messages
    this.consumer = await this.channel.consume(this.name, msg => {
      let done = false
      // console.log('*** msg:', msg)

      // after `this.timeout` seconds NACK the message
      const timeout = setTimeout(() => {
        // console.log('Timeout')
        if ( done ) {
          return
        }
        clearTimeout(timeout)
        done = true
        // console.log('Nacking')
        if ( this.consumer ) {
          this.channel.nack(msg, false, true)
        }
        this.emit('timeout', msg.content.toString())
      }, this.timeout)

      // now emit the event for the user
      // console.log('emitting message')
      this.emit('msg', msg.content.toString(), err => {
        if ( done ) {
          return
        }

        // always do this
        clearTimeout(timeout)
        done = true

        // ToDo: only ack or reject if the channel is still open
        if ( this.consumer ) {
          if (err) {
            this.channel.reject(msg, true)
            return
          }
          this.channel.ack(msg)
        }
      })
    })
    // console.log('consumer:', this.consumer)
  }

  async close() {
    // console.log('closing')
    // if we are consuming messages
    if ( this.consumer ) {
      // close this channel (returns the same as `this.consumer`, so ignore)
      await this.channel.cancel(this.consumer.consumerTag)
      this.consumer = null
    }

    // close the channel
    await this.channel.close()

    // close the connection
    await this.conn.close()
  }
}

// ----------------------------------------------------------------------------

module.exports = Queue

// ----------------------------------------------------------------------------


